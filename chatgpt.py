#!/usr/bin/env python
import requests
import inkex
from lxml import etree
from json import dumps
import re

GPT_PROMPT = """
I would like you to act as an SVG designer. I will ask you to create images, and you will come up with SVG code for the image. Send only the SVG in a code block, so no text. Give me a simple image of XYZ.
"""

REGEX = re.compile(r'(<svg.*>.*</svg>)', re.M|re.S)

OPENAI_API_KEY = "(Enter your API key here)"

class GPT(inkex.EffectExtension):

    def add_arguments(self, pars):
        pars.add_argument(
            "--prompt", help="GPT prompt"
        )
        pars.add_argument(
            "--model", help="Model to use", default="3.5-turbo"
        )

    def effect(self):
        # request the image
        rq = self.request_image()
        # get the svg
        t = etree.fromstring(rq["result"])
        # add it to the document
        self.document.getroot().append(t)

    def request_image(self):
        # make the effective prompt
        real_prompt = GPT_PROMPT.replace("XYZ", self.options.prompt)
        # pick the model
        model = "gpt-" + self.options.model
        # make headers and body
        headers = {
            "Authorization": f"Bearer {OPENAI_API_KEY}",
            "Content-Type": "application/json"
        }
        body = {
            "model": model,
            "messages": [{"role": "user", "content": real_prompt}]
        }
        # send the request and get the response
        response = requests.post("https://api.openai.com/v1/chat/completions", json=body, headers=headers)
        rj = response.json()
        # parse it
        tokens_spent = rj["usage"]["total_tokens"]
        results = rj["choices"]
        result = results[0]["message"]["content"]
        # get the svg code
        matches = re.search(REGEX, result)
        if matches == None:
            return {"tokens": tokens_spent, "result": None}
        return {"tokens": tokens_spent, "result": matches.groups()[0]}

if __name__ == "__main__":
    GPT().run()
